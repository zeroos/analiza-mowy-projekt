from utils.knn import knn
from utils.params import *
from corpora.corpora import CorporaQuery
from corpora.settings import CHILD, ADULT, MALE, FEMALE
from random import shuffle
from scipy import stats
import librosa

#Prosty skrypt wykrywajacy plec mowcy na podstawie f0. Z bazy w folderze
#corpora/corpora_data wybierane sa nagrania doroslych mowcow. Nastepnie sa one
#losowo dzielone na zbior uczacy i testowy w stoskunku 2:1.

def mcqt(sample):
    return librosa.cqt(sample.data,
                       sample.samplerate,
                       n_bins=60,
                       fmin=100.0).mean(axis=1)

def mf0(sample):
    return [amdf_f0(sample.data, sample.samplerate)]

def mifgram(sample):
    freq, D = librosa.core.ifgram(sample.data,
                            sample.samplerate,
                            n_fft=30,
                            hop_length=len(sample.data) + 1,
                            norm=True)
    return freq

def add_params(tr_set_i, ta_set_i, training, target, f):
    if training == []:
        for _ in range(len(tr_set_i)):
            training.append([])

    if target == []:
        for _ in range(len(ta_set_i)):
            target.append([])

    new_tr = []
    new_ta = []

    for i in range(len(tr_set_i)):
        print i, "/", len(tr_set_i)
        new_tr.append(f(tr_set_i[i]))

    for i in range(len(ta_set_i)):
        print i, "/", len(ta_set_i)
        new_ta.append(f(ta_set_i[i]))

    new_tr, new_ta = do_normal(new_tr, new_ta)

    for i in range(len(tr_set_i)):
        training[i].extend(new_tr[i])

    for i in range(len(ta_set_i)):
        target[i].extend(new_ta[i])

    return training, target


print "Getting corpora..."
q = CorporaQuery()
q = q.get_files()
print "Shuffling..."
shuffle(q)
training_set = q[len(q) / 3:]
target_set   = q[:len(q) / 3]

tr_set = []
ta_set = []

print "Getting parameters..."

#wyniki dla af1k1 bc1k1

#oba Accuracy 87.2427983539 %
#samo mifgram Accuracy 56.378600823 %
#samo mcqt Accuracy 87.6543209877 %

# na 3 samo mcqt Accuracy 71.5068493151 %
# na obu Accuracy 75.6164383562 %

tr_set, ta_set = add_params(training_set, target_set, tr_set, ta_set, mcqt)
#tr_set, ta_set = add_params(training_set, target_set, tr_set, ta_set, mf0)
#tr_set, ta_set = add_params(training_set, target_set, tr_set, ta_set, mifgram)

neighs  = []
missess = 0

print "Computing KNN..."

knns = knn(tr_set, 7, ta_set)
for i in range(len(knns)):
    for n in knns[i]:
        neighs.append(training_set[n].voice)

    if stats.mode(neighs)[0][0] != target_set[i].voice:
        missess += 1
        print stats.mode(neighs)[0][0], target_set[i].voice
    neighs = []

print "Accuracy", float(len(target_set) - missess) / len(target_set) * 100.0, "%"
