import numpy
import sys

def DTW( x, y, dist ):
    last = numpy.ndarray( shape = ( 1, len( x ) + 1 ) )
    current = numpy.ndarray( shape = ( 1, len( x ) + 1  ) )

    last[ 0, 0 ] = 0
    for i in range( 1, len( x ) + 1 ):
        last[ 0, i ] = sys.maxint

    for i in range( 0, len( y ) ):
        current[ 0, 0 ] = sys.maxint
        for j in range( 0, len( x ) ):
            cost = dist( x[ j ], y[ i ] )
            current[ 0, j + 1 ] = cost + min( ( last[ 0, j + 1 ], current[ 0, j ], last[ 0, j ] ) )
        current, last = last, current

    return last[ 0, len( x ) ]

def simple_dist( x, y ):
    return 1

def simple_test():
    print DTW( 'test', 'teasgt', simple_dist )

simple_test()
