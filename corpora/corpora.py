import os
import random
import numpy as np
from copy import copy

from corpora_audio import CorporaAudio
from settings import CORPORA_DIR, MALE, FEMALE, CHILD, ADULT
from progressbar import Progressbar

corpora = None


class Corpora:

    def __init__(self):
        if(len(self.list_voices()) == 0):
            print("Nie znaleziono danych. Czy na pewno umiesciles dane z "
                  "korpusu w folderze corpora_data?")
        self._audio_cache = {}

    @classmethod
    def get_instance(cls):
        global corpora
        if corpora is None:
            corpora = Corpora()
        return corpora

    def list_voices(self, gender=None, age=None):
        """ Zwraca liste wszystkich dostepnych glosow """
        all_voices = os.walk(CORPORA_DIR).next()[1]

        option_set = {'k', 'm', 'd', 'c'}
        if gender == FEMALE:
            option_set.intersection_update({'k', 'd'})
        if gender == MALE:
            option_set.intersection_update({'m', 'c'})
        if age == CHILD:
            option_set.intersection_update({'d', 'c'})
        if age == ADULT:
            option_set.intersection_update({'k', 'm'})

        return [v for v in all_voices
                if v[-2] in option_set]

    def list_audio_filenames(self, voice='all'):
        """
        Zwraca liste krotek (voice, filename) gdzie voice to nazwa glosu a
        filename to nazwa pliku audio powiazanego z danym glosem

        @param voice moze byc lista, "all", lub konkretna nazwa glosu
        """

        if(voice == 'all'):
            return self.list_audio_filenames(self.list_voices())

        if(isinstance(voice, str) and
           os.path.isdir(os.path.join(CORPORA_DIR, voice))):
            d, _, filenames = os.walk(
                os.path.join(CORPORA_DIR, voice)).next()
            return [(voice, f) for f in filenames
                    if f.endswith(".wav")]

        return sum([self.list_audio_filenames(v) for v in voice], [])

    def get_audio(self, v, f):
        """
        Returns a CorporaAudio object representing a file f with voice v.
        @param v voice name
        @param f file name
        """

        if (v, f) not in self._audio_cache:
            self._audio_cache[(v, f)] = CorporaAudio(v, f)
        return self._audio_cache[(v, f)]


class CorporaQuery(object):
    params = {}

    def __init__(self, params={}, corpora=None):
        self.corpora = Corpora.get_instance() if corpora is None else corpora
        self.params = params
        self.result = None
        self.test_set = None
        self.train_set = None
        self.validation_set = None
        self._shuffle = True
        self._limit = None
        self._limit_speakers = None

    def __copy__(self):
        q = type(self)(self.params.copy(), self.corpora)
        q.result = None
        q.test_set = None
        q.train_set = None
        q.validation_set = None
        q._shuffle = self._shuffle
        q._limit = self._limit
        q._limit_speakers = self._limit_speakers
        return q

    def filter(self, **kwargs):
        """
        Filtruje zbior wynikow zgodnie z argumentami polecenia, zapisujac
        je w params.
        """
        c = copy(self)
        c.params.update(kwargs)
        return c

    def shuffle(self, shuffle=True):
        """
        Ustawia, czy wyniki powinny byc zwracane w losowej kolejnosci.
        Standardowo tak jest, wiec ta funkcja powinna byc raczej uzawana
        do wylaczenia tego zachowania.

        @param shuffle jesli True pliki beda przelosowywane, wpp. nie.
        """
        c = copy(self)
        c._shuffle = shuffle
        return c

    def limit(self, limit):
        """
        Ustawia limit zwracanych plikow.

        @param limit limit zwracanych plikow, None oznacza brak limitu
        """
        c = copy(self)
        c._limit = limit
        return c

    def limit_speakers(self, limit):
        """
        Ustawia limit mowcow.

        @param limit wsrod zwrocanych plikow CorporaAudio beda nagrania
                     maksymalnie tylu mowcow. None oznacza brak limitu.
        """
        c = copy(self)
        c._limit_speakers = limit
        return c

    def get_files(self):
        """
        Zwraca liste obiektow CorporaAudio spelniajacych wymagania wynikajace
        z self.params.
        """

        if self.result is None:

            voices = self.corpora.list_voices(
                gender=self.params.get("gender", None),
                age=self.params.get("age", None))

            if self._limit_speakers:
                if self._shuffle:
                    random.shuffle(voices)
                voices = voices[:self._limit_speakers]

            l = self.corpora.list_audio_filenames(voice=voices)

            if self._shuffle:
                random.shuffle(l)

            self.result = [self.corpora.get_audio(v, f) for (v, f) in l]

            if "length_lte" in self.params or "length_gte" in self.params:
                length_lte = self.params.get("length_lte", float("inf"))
                length_gte = self.params.get("length_gte", float(0))
                self.result = [a for a in self.result
                               if a.length <= length_lte
                               and a.length >= length_gte]

            if self._limit is not None:
                self.result = self.result[:self._limit]

        return self.result

    def _split_train_data(self, train_ratio=0.6, test_ratio=0.2,
                          speaker_classification=False):
        """
        Wykonuje kwerende i dzieli zbior plikow na trzy podzbiory:
            - zbior uczacy (train_set)
            - zbior walidacyjny (validation_set)
            - zbior testowy (test_set)

        Do pobrania tych zbiorow uzyj funkcji get_{{nazwa_zbioru}}_files()

        @param speaker_classification jesli bedzie ustawiony na False zbiory
               sa tak dobierane, aby w roznych zbiorach nie bylo tego samego
               mowcy (przydatne przy klasyfikacji z nadzorem np. plci).
               W przyciwnym wypadku podzial jest wykonywany tak, zeby w miare
               mozliwosci w roznych zbiorach byly te same glosy  (ale nie ma
               gwarancji, ze to sie uda, bo moze np. zostac wylosowane tylko
               jedno nagranie z danego glosu).
        """
        if self.train_set is not None or self.test_set is not None or \
                self.validation_set is not None:
            print("WARNING: resetting train data split")
        validation_ratio = 1.0 - train_ratio - test_ratio
        assert validation_ratio >= 0

        self.train_set = []
        self.test_set = []
        self.validation_set = []

        train_voices = set()
        test_voices = set()
        validation_voices = set()

        if speaker_classification:
            def contain_test(voice, voices):
                return voice not in voices
        else:
            def contain_test(voice, voices):
                return voice in voices

        for f in self.get_files():
            if contain_test(f.voice, train_voices):
                self.train_set.append(f)
                train_voices.add(f.voice)
            elif contain_test(f.voice, test_voices):
                self.test_set.append(f)
                test_voices.add(f.voice)
            elif contain_test(f.voice, validation_voices):
                self.validation_set.append(f)
                validation_voices.add(f.voice)
            else:
                s = float(len(train_voices) + len(test_voices) +
                          len(validation_voices))

                if s == 0:
                    s = 1

                r_train = (len(train_voices)/s) - train_ratio
                r_test = (len(test_voices)/s) - test_ratio
                r_validation = (len(validation_voices)/s) - validation_ratio

                m = min(r_train, r_test, r_validation)
                if r_train == m:
                    self.train_set.append(f)
                    train_voices.add(f.voice)
                elif r_test == m:
                    self.test_set.append(f)
                    test_voices.add(f.voice)
                elif r_validation == m:
                    self.validation_set.append(f)
                    validation_voices.add(f.voice)

    def get_train_files(self):
        if self.train_set is None:
            self._split_train_data()
        return self.train_set

    def get_test_files(self):
        if self.test_set is None:
            self._split_train_data()
        return self.test_set

    def get_validation_files(self):
        if self.validation_set is None:
            self._split_train_data()
        return self.validation_set

    def fill_cache(self, name, func, silent=False):
        """
        Inserts a result of `func` applied to all files in this query
        to those files' cache.

        @param name name of cache entity under which the result would be stored
        @param func function that takes CorporaQuery as a parameter and returns
                    some data
        @param silent if set to True, ipython progressbar would not show up
        """
        files = self.get_files()
        if not silent:
            progress = Progressbar(len(files))

        for i, ca in enumerate(files):
            ca.cache[name] = func(ca)
            if not silent:
                progress.update(el_i=i+1)

    def save_cache(self, filename, cache_list=None):
        """
        Saves a cache entities of all files in given query to file in a NPZ
        format.

        @param filename name of a file to create
        @cache_list list of cache names to save. If None, saves all caches.
        """
        data = {}

        if cache_list is None:
            cache_all = True
        else:
            cache_all = False

        for ca in self.get_files():
            if cache_all:
                cache_list = ca.cache.keys()
            for cache_name in cache_list:
                if cache_name in ca.cache:
                    data['/'.join([ca.voice, ca.filename, cache_name])] = \
                        ca.cache[cache_name]
        np.savez(filename, **data)

    def load_cache(self, filename, cache_list, silent=False):
        """
        Loads cache from file and stores it in coresponding CorporaAudio objects
        in this query (and only from this query)
        """
        npz = np.load(filename)
        files = self.get_files()

        if not silent:
            progress = Progressbar(len(files))

        for i, ca in enumerate(files):
            for cache_name in cache_list:
                key = '/'.join([ca.voice, ca.filename, cache_name])
                if key in npz:
                    ca.cache[cache_name] = npz[key]
            if not silent:
                progress.update(el_i=i+1)


if __name__ == "__main__":
    q = CorporaQuery().filter(age=CHILD, gender=FEMALE)
    print(q.get_data())
