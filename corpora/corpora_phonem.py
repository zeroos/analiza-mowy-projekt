import os
import random

from corpora import Corpora, CorporaQuery
from corpora_audio import CorporaAudio

from settings import CORPORA_DIR

class CorporaPhonem(CorporaQuery):
    voices_phonems = {}
    loaded_files   = None

    def get_pif(self, voice, filename):
        """
        Zwraca liste fonemow wraz z poczatkiem i koncem dla wybranego pliku.
        Zaklada, ze plik .mlf jest poprawny (todo?).
        """
        mlf_path = os.path.join(CORPORA_DIR, voice, voice + ".mlf")

        try:
            mlf_file = open(mlf_path, "r")

        except:
            print "Problem with opening: ", mlf_path
            return []

        result = []
        found_phonems = []

        while True:
            line = mlf_file.readline()

            if line == '':
                break

            if filename[:-3] in line:
                while True:
                    phonem_line = mlf_file.readline()

                    if phonem_line == ".\n":
                        break
                    else:
                        found_phonems.append(phonem_line.split())
                break

        for p in found_phonems:
            result.append([p[2], int(p[0]), int(p[1])])

        mlf_file.close()

        return result

    def init_phonems_list(self):
        """
        Zbiera informacje o dostepnych fonemach z zaladowanych plikow.
        """
        self.loaded_files = self.get_files()

        for i in range(len(self.loaded_files)):
            phonems_in_file = self.get_pif(self.loaded_files[i].voice,
                                           self.loaded_files[i].filename)

            for phonem, begin, end in phonems_in_file:
                begin = begin * self.loaded_files[i].samplerate / 10000000
                end   = end * self.loaded_files[i].samplerate / 10000000

                if phonem in self.voices_phonems:
                    self.voices_phonems[phonem].append([i, [begin, end]])
                else:
                    self.voices_phonems[phonem] = [[i, [begin, end]]]

    def get_phonems(self, phonem, count=1):
        """
        Zwraca liste wave'ow podanego fonemu. Jesli fonem nie wystepuje
        w zaladowanych plikach, to zwraca pusta liste i wypisuje komunikat.

        @param phonem string z szukanym fonemem
        @param count ile razy ma losowac z dostepnych fonemow
        """
        if not self.voices_phonems or self.loaded_files == None:
            self.voices_phonems = {}
            self.loaded_files   = None
            self.init_phonems_list()

        result = []

        if phonem not in self.voices_phonems:
            print "Phonem {} not found in loaded files".format(phonem)
            return result

        for _ in range(count):
            voice_index, phonem_range = random.choice(self.voices_phonems[phonem])

            wave = self.loaded_files[voice_index].data

            result.append(wave[phonem_range[0]:phonem_range[1]])

        return result
