import os

CORPORA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "corpora_data")
MALE = 'm'
FEMALE = 'f'
CHILD = 'c'
ADULT = 'a'

MALE_CLASS = 0
FEMALE_CLASS = 1
CHILD_CLASS = 2
ADULT_CLASS = 3
BOY_CLASS = 4
GIRL_CLASS = 5
MAN_CLASS = 6
WOMAN_CLASS = 7

UNKNOWN_CLASS = -1
