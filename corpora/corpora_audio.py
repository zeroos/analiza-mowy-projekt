import os
import librosa

from settings import CORPORA_DIR, MALE, FEMALE, CHILD, ADULT


class CorporaAudio:
    parameters = None  # dict
    cache = None  # dict
    filename = None
    voice = None

    def __init__(self, voice, filename):
        self.cache = {}
        p = {}
        p["age"] = CHILD if voice[-2] in {'c', 'd'} else ADULT
        p["gender"] = FEMALE if voice[-2] in {'k', 'd'} else MALE
        self.parameters = p

        self.voice = voice
        self.filename = filename

        self.filepath = os.path.join(CORPORA_DIR, voice, filename)

    def load_audio(self):
        data, samplerate = librosa.load(self.filepath)
        self.cache['data'], self.cache['samplerate'] = data, samplerate

    def unload_audio(self):
        try:
            self.cache.pop('data')
            self.cache.pop('samplerate')
        except KeyError:
            pass  # audio not loaded

    def audio_loaded(self):
        return "data" in self.cache

    @property
    def data(self):
        if 'data' not in self.cache:
            self.load_audio()

        return self.cache['data']

    @property
    def samplerate(self):
        if 'samplerate' not in self.cache:
            self.load_audio()

        return self.cache['samplerate']

    @property
    def length(self):
        """
        Zwraca dlugosc nagrania, w sekundach.

        UWAGA: aby policzyc dlugosc konieczne jest wczytanie pliku z dysku,
        co zajmuje czas i zasoby. Jesli plik wczesniej nie byl wczytany
        to po policzeniu dlugosci zostanie 'odwczytany'.
        """
        if 'length' not in self.cache:
            audio_was_loaded = self.audio_loaded()

            self.cache['length'] = len(self.data)/float(self.samplerate)

            if not audio_was_loaded:
                self.unload_audio()

        return self.cache['length']

    def __repr__(self):
        return "<G: {} A: {} ({})>".format(
            self.parameters['gender'],
            self.parameters['age'],
            self.filename
        )
