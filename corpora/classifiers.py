from settings import MALE, FEMALE, CHILD, ADULT, \
    MALE_CLASS, FEMALE_CLASS, CHILD_CLASS, ADULT_CLASS, \
    BOY_CLASS, GIRL_CLASS, MAN_CLASS, WOMAN_CLASS, \
    UNKNOWN_CLASS


def classify_gender(corpora_audio):
    """
    Przyporzadkowuje obiekt CorporaAudio do klasy MALE lub FEMALE.

    @returns int MALE_CLASS, FEMALE_CLASS lub -1 jesli nie udalo sie
    przyporzadkowac klasy.
    """
    m = {MALE: MALE_CLASS,
         FEMALE: FEMALE_CLASS}
    default = UNKNOWN_CLASS
    return m.get(corpora_audio.parameters.get("gender"), default)


def classify_age(corpora_audio):
    """
    Przyporzadkowuje obiekt CorporaAudio do klasy CHILD lub ADULT.

    @returns int CHILD_CLASS, ADULT_CLASS lub -1 jesli nie udalo sie
    przyporzadkowac klasy.
    """

    m = {CHILD: CHILD_CLASS,
         ADULT: ADULT_CLASS}
    default = UNKNOWN_CLASS
    return m.get(corpora_audio.parameters.get("age"), default)


def classify_age_and_gender(corpora_audio):
    """
    Przyporzadkowuje obiekt CorporaAudio do klasy BOY, GIRL, MAN, WOMAN.

    @returns int BOY_CLASS, GIRL_CLASS, MAN_CLASS, WOMAN_CLASS lub -1 jesli nie
    udalo sie przyporzadkowac klasy.
    """

    age = classify_age(corpora_audio)
    gender = classify_gender(corpora_audio)

    if age == UNKNOWN_CLASS or gender == UNKNOWN_CLASS:
        return UNKNOWN_CLASS

    m = {
        CHILD_CLASS: {MALE_CLASS: BOY_CLASS, FEMALE_CLASS: GIRL_CLASS},
        ADULT_CLASS: {MALE_CLASS: MAN_CLASS, FEMALE_CLASS: WOMAN_CLASS}
    }
    return m[age][gender]


def classify_speaker_factory(corpora_query):
    """
    Funkcja na podstawie obiektu klasy CorporaQuery zwraca funkcje, ktora
    klasyfikuje mowcow z tego CorporaQuery. CorporaQuery zostanie wykonane.
    """

    s = set()  # set of all voices in given query
    classes = dict()  # voice name -> number

    for f in corpora_query.get_files():
        if f.voice not in s:
            classes[f.voice] = len(classes)
            s.add(f.voice)

    def classify_speaker(corpora_audio):
        """
        Zwraca klase odpowiadajaca mowcy danego CorporaAudio.
        """
        return classes[corpora_audio.voice]

    return classify_speaker
