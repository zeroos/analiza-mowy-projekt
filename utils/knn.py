import numpy as np

def knn(data_set, k, elements):
    #data_set, elements to listy wektorow parametrow. Koszt to odleglosc
    #euklidesowa pomiedzy dwoma wektorami. data_set to zbior uczacy, elements
    #to zbior testowy
    x = np.reshape(data_set, [1,len(data_set),len(data_set[0])])
    y = np.reshape(elements, [len(elements), 1, len(elements[0])])
    
    diff_mtx = x - y
    
    dists = (diff_mtx ** 2).sum(axis = 2)
    argsorted = np.argsort(dists)
    result    = []

    for i in range(len(argsorted)):
        result.append(argsorted[i][:k])

    return result
