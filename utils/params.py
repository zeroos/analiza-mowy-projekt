import numpy as np

MIN_F0 = 50.0
MAX_F0 = 500.0


def amdf_f0(sound, sr):
    """
    Oblicza wartosc f0 dla calej probki sound przy pomocy AMDF
    @param sr to czestotliwosc probkowania
    """
    min_delay = int(sr / MAX_F0)
    max_delay = int(sr / MIN_F0)
    min_amdf = None
    result = []

    for m in range(min_delay, max_delay):
        amdf_val = 0
        for n in range(len(sound) - m):
            amdf_val += abs(sound[n] - sound[n + m])
        if min_amdf is None:
            min_amdf = (amdf_val, m)
        elif min_amdf[0] > amdf_val:
            min_amdf = [amdf_val, m]

        result.append(amdf_val)

    return float(sr) / min_amdf[1]


def fft_hamming(sound, _):
    """
    zwraca FFT na sygnale po nalozeniu okna Hamminga
    """
    h_mult = np.hamming(len(sound))
    wsound = []

    for i in range(len(sound)):
        wsound.append(sound[i] * h_mult[i])

    return np.fft.fft(wsound)


def windowed(f, sound, sr, window_len=0.1):
    """
    Pozwala wywolac amdf_f0 lub fft_hamming na sygnale podzielonym na ramki.
    @param f to funkcja, ktora chcemy zastosowac
    @param sound to nasz sygnal jako lista floatow
    @param sr to czestotliwosc probkowania
    @param window_len to dlugosc okna w sekundach
    """
    result = []
    w_in_sampl = window_len * sr
    w_begin = 0

    while w_begin + w_in_sampl < len(sound):
        result.append(f(sound[w_begin:w_begin + w_in_sampl], sr))
        w_begin += w_in_sampl

    return result


def do_normal(tr_array, ta_array):
    """
    Normalizuje wartosci w tablicach tr_array i ta_array do wartosci
    z przedzialu 0..1.
    """
    tr_array = np.array(tr_array)
    ta_array = np.array(ta_array)

    max_val_tr = np.max(tr_array)
    max_val_ta = np.max(ta_array)
    max_val = np.max([max_val_tr, max_val_ta])

    for v in tr_array:
        v /= float(max_val)

    for v in ta_array:
        v /= float(max_val)

    return tr_array, ta_array


def data_transformed(data, target, feature_extractor):
    """
    Funkcja przerabia dane oraz liste celow (target) na odpowiednie dane
    przeksztalcone za pomoca feature_extractor.

    @param data dane do przeksztalcenia
    @param target lista celow
    @param feature_extroctar librosa.util.FeatureExtractor ktorym
                             przeksztalcane sa dane
    @returns np.array celow o takim samym wymiarze jak
                      feature_extractor.transform(data)
    """
    # TODO: zintegrowac te funkcje jakos w Pipeline
    result = np.empty((0,))
    transformed_data = feature_extractor.transform(data)
    for r, t in zip(transformed_data, target):
        result = np.append(result, [t]*r.shape[1])
    return (transformed_data, result)
