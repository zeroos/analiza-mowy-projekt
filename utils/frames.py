import numpy as np
import librosa
import scipy


def get_frames(corpora_audio, target,
               frame_length=511, hop_length=64, window=np.hamming):
    """
    Dzieli audio na nachodzace na siebie ramki o podanej dlugosci,
    przepuszczone przez zadane okno. Dlugosc ramki musi byc mniejsza,
    niz ta w przeksztalceniach (np CQT). Zwraca takze liste
    z podana dla danego nagrania klasa.
    """
    data_frames = librosa.util.frame(corpora_audio.data,
                                     frame_length, hop_length).T
    targets = []

    for i in range(len(data_frames)):
        wmult = window(len(data_frames[i]))
        data_frames[i] *= wmult
        targets.append(target)

    return data_frames, np.array(targets)


def get_data_from_cache_func_factory(cache_name):
    """
    Zwraca funkcje do wykorzystania jako `get_data_func` z metody `prep_data`,
    ktora dla pliku corpora audio zwraca dane z zadanego cache'a. Jesli
    dany cache nie istnieje, zostanie zgloszony wyjatek.

    @param cache_name nazwa cache'a z ktorego maja byc zwracane dane
    """
    def get_data_from_cache(corpora_audio, target, *_):
        data_array = corpora_audio.cache[cache_name]
        target_array = np.full(data_array.shape[0], target)
        return data_array, target_array
    return get_data_from_cache


def prep_data(files,
              classify_func,
              get_data_func=get_frames,
              frame_length=511, hop_length=64, window=np.hamming):
    """
    Dla listy obiektow CorporaAudio zwraca dane podzielone na ramki,
    liste klas odpowiadajacych kolejnym ramkom
    oraz liste krotek z informacja o liczbie ramek w kolejnych plikach
    i ich nazwach.
    """
    all_frames  = []
    all_targets = []
    frame_info  = []

    for f in files:
        x, y         = get_data_func(f, classify_func(f), frame_length, hop_length, window)
        all_frames  += x.tolist()
        all_targets += y.tolist()
        frame_info.append([len(x), f.filename])

    return np.array(all_frames), np.array(all_targets), frame_info


def get_score(preds, target, frame_info, silent_mode=True):
    """
    Podaje wynik klasyfikacji, patrzac na mode przewidywanych
    klas w ramach jednego pliku.

    @param preds lista klas zwroconych z pipe'a
    @param target rzeczywiste klasy dla probek
    @param framenums lista par, gdzie pierwszy elemento to informacja,
           ile kolejnych ramek nalezy do jednego pliku, a drugi to jego nazwa
    @param silent_mode okresla, czy chcemy dostac informacje
           o zlych klasyfikacjach (False), badz nie (True)
    """
    prev_iter = 0
    act_iter  = 0
    counter   = 0
    missess   = 0

    while act_iter < len(preds):
        prev_iter = act_iter
        act_iter += frame_info[counter][0]

        act_pred = scipy.stats.mode(preds[prev_iter:act_iter])[0][0]

        if act_pred != target[prev_iter]:
            missess += 1.0
            if not silent_mode:
                print "{} from class {} classified as {}".format(frame_info[counter][1],
                                                                 target[prev_iter],
                                                                 act_pred)

        counter += 1

    return 1.0 - missess / len(frame_info)
