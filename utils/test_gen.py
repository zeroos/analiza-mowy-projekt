import os
import librosa
import random
import numpy

import sys
sys.path.append("..") # dodaj folder wyzej do sciezki szukania

from corpora.settings import CORPORA_DIR, MALE, FEMALE, CHILD, ADULT
from corpora.corpora import CorporaQuery

def get_test(test_len = 5, hard_mode = True):
    """
    Losuje test i zwraca go jako tekst, wzorzec
    i poczatek wzorca w tekscie (w liczbie probek).
    Ostatni parametr to samplerate.
    @param test_len to liczba plikow w tekscie
    @param hard_mode okresla, czy wzorzec ma pochodzic od innego mowcy
    """
    result  = []
    pattern = []
    answer  = 0
    q       = CorporaQuery().limit(test_len)
    files   = q.get_files()

    for f in files:
        result += f.data.tolist()

    pattern_i = random.choice(range(test_len))

    for i in range(pattern_i):
        answer += len(files[i].data)

    voice     = random.choice(q.corpora.list_voices()) # wybierz mowce do wzorca
    pattern   = files[pattern_i].data # jesli nie znajdzie sie slowo od innego mowcy

    pattern_filename = files[pattern_i].filename[len(files[pattern_i].voice):]

    for s in q.corpora.list_audio_filenames([voice]):
        if s[1].endswith(pattern_filename):
            filepath = os.path.join(CORPORA_DIR, s[0], s[1])
            data, sr = librosa.load(filepath)
            pattern  = data
            break

    return result, pattern, answer, files[0].samplerate
