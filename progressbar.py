import uuid
from IPython.display import HTML, Javascript, display


class Progressbar:

    def __init__(self, el_num=None):
        self.divid = str(uuid.uuid4())
        self.proc = 0
        self.el_num = el_num

        self.pb = HTML(
            """
            <div style="border: 1px solid black; width:500px">
            <div id="%s" style="transition: width 1s;
                                background-color:blue;
                                width:0%%">&nbsp;</div>
            </div>
            """ % self.divid)
        display(self.pb)

    def update(self, proc=None, el_i=None):
        if el_i is not None:
            proc = el_i * 100 / self.el_num

        if self.proc == proc:
            return

        self.proc = proc
        display(Javascript("$('div#%s').width('%i%%')" % (self.divid, proc)))
