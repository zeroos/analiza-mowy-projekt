import os
import sys
import librosa
import random
import numpy
from corpora.corpora import CorporaQuery

DATA_NUM    = 10
DATA_LEN    = 5
SILENCE_MIN = 0
SILENCE_MAX = 0
HARD_MODE   = True
DATA_PATH   = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "pattern_data")

def get_test_sample(samples, data_len, diff=True):
    #@param data_len okresla, ile poczatkowych probek nalezy do danych
    #@param diff okresla, czy probka testowa moze byc innego mowcy, niz dana
    #jesli takiej nie znajdzie, to zwraca nagranie wystepujace w danych

    test_sample = random.choice(samples[:data_len])

    if not diff:
        return test_sample

    test_filename = test_sample.filename[len(test_sample.voice):]

    for s in samples[data_len:]:
        if s.filename.endswith(test_filename):
            return s

    return test_sample

for p in sys.argv:
    if p[0] == 'n':
        DATA_NUM = int(p[2:])
    if p[0] == 'l':
        DATA_LEN = int(p[2:])
    if p[0] == 'h':
        HARD_MODE = int(p[2:])

q = CorporaQuery().get()

sample_rate = q[0].samplerate

silence     = [0.0]
# Compute min and max num of zeros between waves
silence_min = int(SILENCE_MIN * 1.0 / sample_rate)
silence_max = int(SILENCE_MAX * 1.0 / sample_rate)

for i in range(DATA_NUM):
    audio_data = []

    random.shuffle(q)

    for sample_i in range(DATA_LEN):
        audio_data += q[sample_i].data.tolist()
        audio_data += silence * random.randint(silence_min, silence_max)

    librosa.output.write_wav(os.path.join(DATA_PATH, str(i) + "_data.wav"),
                             numpy.array(audio_data), sample_rate)

    test_sample = get_test_sample(q, DATA_LEN, HARD_MODE)
    librosa.output.write_wav(os.path.join(DATA_PATH, str(i) + "_test.wav"),
                             test_sample.data, sample_rate)
